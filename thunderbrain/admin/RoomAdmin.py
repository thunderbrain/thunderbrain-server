from django import forms
from django.contrib import admin

from thunderbrain.models import Room

class RoomAdminForm(forms.ModelForm):
	adminPassword = forms.CharField(widget = forms.PasswordInput, required = True)

	class Meta:
		model = Room
		exclude = ('roomURI', )

class RoomAdmin(admin.ModelAdmin):
	form = RoomAdminForm
