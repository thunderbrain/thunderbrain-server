from django.contrib import admin

from thunderbrain.models import Comment
from thunderbrain.models import Idea
from thunderbrain.models import Room

from .RoomAdmin import RoomAdmin

admin.site.register(Comment)
admin.site.register(Idea)
admin.site.register(Room, RoomAdmin)
