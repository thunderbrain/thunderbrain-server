# import os
from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
from channels.generic.websocket import AsyncWebsocketConsumer
from django.conf.urls import url

from thunderbrain import consumers

# pylint: disable=invalid-name
application = ProtocolTypeRouter({
	# (http->django views is added by default)
	'websocket': AuthMiddlewareStack(URLRouter([
		url(r'^room/(?P<roomURI>\w+)$', consumers.RoomConsumer),
		url('', AsyncWebsocketConsumer),
	])),
})
