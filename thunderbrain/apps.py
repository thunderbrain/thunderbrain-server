from django.apps import AppConfig

class ThunderbrainConfig(AppConfig):
	name = 'thunderbrain'
