from channels.generic.websocket import AsyncJsonWebsocketConsumer
from django.utils import timezone
from thunderbrain.models import Room, Idea, Comment, serializeUser

class RoomConsumer(AsyncJsonWebsocketConsumer):
	async def sendError(self, msg):
		# Send error to the connected user
		await self.send_json({
			'event': 'error',
			'payload': {
				'reason': msg
			}
		})

	async def sendEventToClient(self, event):
		# del event['type']
		await self.send_json(event)

	event_commentAdded = sendEventToClient
	event_ideaAdded = sendEventToClient
	event_roomPhaseChanged = sendEventToClient
	event_roomSettingsChanged = sendEventToClient
	event_userJoined = sendEventToClient
	event_userLeft = sendEventToClient

	async def connect(self):
		# pylint: disable=attribute-defined-outside-init

		user = self.scope['user']
		# self.onlineUsers.append(user)
		print(f'[RoomConsumer] connecting user: {user}')

		roomURI = self.scope['url_route']['kwargs']['roomURI']
		try:
			self.room = Room.objects.get(roomURI=roomURI)
		except Room.DoesNotExist:
			print(f'Requested room not found: {roomURI}')
			self.close(404)
			return

		self.roomGroupName = self.room.getRoomGroupName()

		# Join room group
		print(f'[RoomConsumer] joining group {self.roomGroupName}')
		await self.channel_layer.group_add(
			self.roomGroupName,
			self.channel_name
		)

		# TODO: verify; if phase = created - accept all and add to the list; otherwise reject?
		if user in self.room.users.all():
			await self.accept()
		else:
			await self.close(403)
			return

		# Send room data
		await self.send_json({
			'event': 'connected',
			'payload': {
				'ideas': [i.serialize() for i in self.room.ideas.all()],
				# TODO: Users or current online users?
				'users': [serializeUser(u) for u in self.room.users.all()],
				'roomSettings': self.room.serializeRoomSettings(),
				'roomState': self.room.serializeRoomState(),
			}
		})
		await self.channel_layer.group_send(
			self.roomGroupName,
			{
				'type': 'event.userJoined',
				'event': 'userJoined',
				'payload': serializeUser(user)
			}
		)

	async def receive_json(self, content, **kwargs):
		# Update room
		roomURI = self.room.roomURI
		try:
			self.room = Room.objects.get(roomURI=roomURI)
		except Room.DoesNotExist:
			print(f'Requested room not found: {roomURI}')
			self.close(404)
			return

		groupMessage = {}

		function = content.get('function')
		params = content.get('params')
		user = self.scope['user']

		try:
			if function == 'changeRoomSettings':
				if self.room.adminPassword != params['adminPassword']:
					await self.sendError("Wrong admin password")
					return

				# Valid password - changing settings
				if 'name' in params:
					self.room.name = params.get('name')
				if 'ideasTime' in params:
					self.room.ideasTime = int(params.get('ideasTime'))
				if 'commentsTime' in params:
					self.room.commentsTime = int(params.get('commentsTime'))
				if 'roomTTL' in params:
					self.room.roomTTL = int(params.get('roomTTL'))
				groupMessage['event'] = 'roomSettingsChanged'
				groupMessage['payload'] = self.room.serializeRoomSettings()

			elif function == 'addIdea':
				if self.room.phase != self.room.RoomPhase.ideas.value:
					await self.sendError("Wrong phase for this function")
					return

				# TODO verify its saved and when it should be saved
				newIdea = Idea(
					createdTimestamp=timezone.now(),
					author=user,
					room=self.room,
					text=params.get('text', '')
				)
				newIdea.save()
				groupMessage['event'] = 'ideaAdded'
				groupMessage['payload'] = newIdea.serialize()

			elif function == 'addComment':
				if self.room.phase != self.room.RoomPhase.comments.value:
					await self.sendError("Wrong phase for this function")
					return
				# Make sure idea exists
				commentedIdea = Idea.objects.get(ideaID=int(params.get('ideaID')))
				newComment = Comment(
					createdTimestamp=timezone.now(),
					author=user,
					idea=commentedIdea,
					text=params.get('text'),
					rating=int(params.get('rating'))
				)

				try:
					# Retrieve previous comment to invalidate its rating if any
					previousComment = Comment.objects.filter(author=user, idea=commentedIdea).latest('createdTimestamp')
					ratingUpdate = -previousComment.rating
				except Comment.DoesNotExist:
					ratingUpdate = 0
				ratingUpdate += newComment.rating
				commentedIdea.rating += ratingUpdate

				# Save comment and idea
				newComment.save()
				commentedIdea.save()

				groupMessage['event'] = 'commentAdded'
				groupMessage['payload'] = {
					"ideaID": commentedIdea.ideaID,
					"ratingChange": ratingUpdate,
					"comment": newComment.serialize()
				}

			elif function == 'changeRoomPhase':
				if self.room.adminPassword != params['adminPassword']:
					await self.sendError("Wrong admin password")
					return
				self.room.setRoomPhase(params['newPhase'])

				groupMessage['event'] = 'roomPhaseChanged'
				groupMessage['payload'] = {
					'newPhase': self.room.phase,
					# FIXME - its not really now - it should be returned by setRoomPhase function.
					'timestamp': timezone.now().isoformat(' '),
				}

			else:
				await self.sendError("Unknown function")
				return
		# expecting KeyError, Django db error, parsing error (string - expected int)
		except Exception as e:
			print(f'[RoomConsumer] error: {e}')
			await self.sendError(f"Wrong parameters in {function} call")
			return

		# Send message to room group
		groupMessage['type'] = 'event.' + groupMessage['event']
		await self.channel_layer.group_send(
			self.roomGroupName,
			groupMessage
		)
		self.room.save()

	async def disconnect(self, code):
		groupMessage = {
			'type': 'event.userLeft',
			'event': 'userLeft',
			'payload': serializeUser(self.scope['user'])
		}

		# TODO update online users list?
		await self.channel_layer.group_send(
			self.roomGroupName,
			groupMessage
		)

		# Leave room group
		await self.channel_layer.group_discard(
			self.roomGroupName,
			self.channel_name
		)
