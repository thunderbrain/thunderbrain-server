"""
Thunderbrain URL Configuration
"""

import inspect
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path

from thunderbrain import views

urlpatterns = [
	path('admin/', admin.site.urls),
]

# Generate urls for all elements in 'views'
urlpatterns += [path(viewTuple[0], viewTuple[1]) for viewTuple in inspect.getmembers(views, inspect.isfunction)]

# Add static files
urlpatterns += staticfiles_urlpatterns()
