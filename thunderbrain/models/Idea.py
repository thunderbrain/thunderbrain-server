from django.conf import settings
from django.db import models
from django.utils.timezone import now
from .serializeUser import serializeUser

class Idea(models.Model):
	"""A brainstorm idea"""

	ideaID = models.AutoField(primary_key=True)
	createdTimestamp = models.DateTimeField()
	author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='ideas')
	room = models.ForeignKey('Room', on_delete=models.CASCADE, related_name='ideas')
	text = models.TextField()
	rating = models.IntegerField(default=0)

	def save(self, *args, **kwargs):
		# Make sure that pk is set
		if not self.ideaID:
			self.createdTimestamp = now()
		super().save(*args, **kwargs)

	def serialize(self):
		return {
			'ideaID': self.ideaID,
			'author': serializeUser(self.author),
			'text': self.text,
			'rating': self.rating,
			'comments': [c.serialize() for c in self.comments.all()],
			'createdTimestamp': str(self.createdTimestamp) if self.createdTimestamp else None,
		}
