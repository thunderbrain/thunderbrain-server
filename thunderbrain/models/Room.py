import uuid
from django.conf import settings
from django.db import models
from enum import Enum
from django.utils.timezone import now, timedelta
from thunderbrain.tasks import proceedRoomPhaseTask


class Room(models.Model):
	"""A brainstorm room"""

	class RoomPhase(Enum):
		created = 0
		ideas = 1
		comments = 2
		locked = 3
		expired = 4

	roomID = models.AutoField(primary_key=True)
	roomURI = models.CharField(blank=True, null=False, unique=True, max_length=32)
	adminPassword = models.CharField('Room admin password', blank=False, null=False, max_length=80)
	name = models.CharField('Room name', blank=False, null=False, max_length=80)
	commentsPhaseTimestamp = models.DateTimeField(null=True)
	commentsTime = models.IntegerField(default=0)
	ideasPhaseTimestamp = models.DateTimeField(null=True)
	ideasTime = models.IntegerField(default=0)
	lockPhaseTimestamp = models.DateTimeField(null=True)
	roomTTL = models.IntegerField(default=0)
	users = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True)
	phase = models.IntegerField(default=RoomPhase.created.value, choices=[(tag, tag.value) for tag in RoomPhase])

	@staticmethod
	def generateUUID(room):
		"""
		Generate room URI: uuid v5 of namespace plus room ID plus room name, with dashes skipped
		"""
		if room.roomID:
			return uuid.uuid5(settings.ROOM_NAMESPACE, f'{room.roomID}').hex
		return ''

	def save(self, *args, **kwargs):
		# Make sure that pk is set
		if not self.roomID:
			super().save(*args, **kwargs)

		# If missing, generate and save the URI/UUID
		if not self.roomURI and self.roomID:
			self.roomURI = Room.generateUUID(self)
		super().save(*args, **kwargs)

	def serializeRoomSettings(self):
		return {
			'roomTTL': self.roomTTL,
			'ideasTime': self.ideasTime,
			'commentsTime': self.commentsTime,
			'name': self.name,
		}

	def serializeRoomState(self):
		return {
			'phase': self.phase,
			'ideasPhaseTimestamp': str(self.ideasPhaseTimestamp) if self.ideasPhaseTimestamp else None,
			'commentsPhaseTimestamp': str(self.commentsPhaseTimestamp) if self.commentsPhaseTimestamp else None,
			'lockPhaseTimestamp': str(self.lockPhaseTimestamp) if self.lockPhaseTimestamp else None,
		}

	def setRoomPhase(self, phase):
		reverse = (phase - self.phase) < 0
		for i in range(abs(phase - self.phase)):
			self.proceed(reverse=reverse)

	def proceed(self, reverse=False):
		# TODO in theory task scheduling should be done on room.save; however we don't modify this field without save
		# as it has to make sure that the phase change is commited. some flag should be used
		# non critical
		if not reverse:
			schedule = None
			if self.phase < Room.RoomPhase.expired.value:
				self.phase += 1
			if self.phase == Room.RoomPhase.ideas.value:
				self.ideasPhaseTimestamp = now()
				if self.ideasTime > 0:
					schedule = self.ideasPhaseTimestamp + timedelta(seconds=self.ideasTime)
			elif self.phase == Room.RoomPhase.comments.value:
				self.commentsPhaseTimestamp = now()
				if self.commentsTime > 0:
					schedule = self.commentsPhaseTimestamp + timedelta(seconds=self.commentsTime)
			elif self.phase == Room.RoomPhase.locked.value:
				self.lockPhaseTimestamp = now()
				if self.roomTTL > 0:
					schedule = self.lockedPhaseTimestamp + self.roomTTL
			if schedule is not None:
				proceedRoomPhaseTask(
					self.roomID,
					self.phase,
					schedule=schedule
				)
		else:
			# todo - here the background task should be removed - but its not critical as  this functionality is not used
			if self.phase == Room.RoomPhase.ideas.value:
				self.phase = Room.RoomPhase.created.value
				self.ideasPhaseTimestamp = None
			elif self.phase == Room.RoomPhase.comments.value:
				self.phase = Room.RoomPhase.ideas.value
				self.commentsPhaseTimestamp = None
			elif self.phase == Room.RoomPhase.locked.value:
				self.phase = Room.RoomPhase.comments.value
				self.lockedPhaseTimestamp = None
			if self.phase > Room.RoomPhase.created.value:
				self.phase -= 1
			# expired not handled - it shouldnt be allowed

	def getRoomGroupName(self):
		return 'room-' + str(self.roomID)

	def __str__(self):
		return f'Room {self.roomID} [{self.roomURI}]: {self.name}'
