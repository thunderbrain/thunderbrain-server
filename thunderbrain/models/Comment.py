from django.conf import settings
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils.timezone import now
from .serializeUser import serializeUser

class Comment(models.Model):
	"""A comment for a brainstorm idea."""

	commentID = models.AutoField(primary_key=True)
	createdTimestamp = models.DateTimeField()
	author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='comments')
	idea = models.ForeignKey('Idea', on_delete=models.CASCADE, related_name='comments')
	text = models.TextField()
	rating = models.IntegerField(
		default=0,
		validators=[MinValueValidator(-1), MaxValueValidator(1)],
	)

	def save(self, *args, **kwargs):
		# Make sure that pk is set
		if not self.commentID:
			self.createdTimestamp = now()
		super().save(*args, **kwargs)

	def serialize(self):
		return {
			'commentID': self.commentID,
			'author': serializeUser(self.author),
			'rating': self.rating,
			'text': self.text,
			'createdTimestamp': str(self.createdTimestamp) if self.createdTimestamp else None,
		}
