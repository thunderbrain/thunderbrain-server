def serializeUser(user):
    return {
        "username": user.username,
        "firstName": user.first_name,
        "lastName": user.last_name,
    }
