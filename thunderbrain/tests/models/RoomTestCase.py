from django.test import TestCase
from thunderbrain.models import Room

class RoomTestCase(TestCase):
	def setUp(self):
		room1 = Room(name="food", adminPassword="pass1")
		room1.save()
		room2 = Room(name="drinks", adminPassword="pass2")
		room2.save()

	def test_room_generates_uri(self):
		"""Rooms should generate their URIs automatically on create"""
		room1 = Room.objects.get(name="food")
		room2 = Room.objects.get(name="drinks")
		room1URI = Room.generateUUID(room1)
		room2URI = Room.generateUUID(room2)
		self.assertEqual(room1.roomURI, room1URI)
		self.assertEqual(room2.roomURI, room2URI)
