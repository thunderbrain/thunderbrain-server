import json
from django.contrib import auth
from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseBadRequest

def registerUser(request):
	if request.method != 'POST':
		return HttpResponseBadRequest()
	if request.user.is_authenticated:
		return HttpResponseBadRequest()

	try:
		data = json.loads(request.body)
	except json.JSONDecodeError:
		return HttpResponseBadRequest()

	username = data.get('username', '').strip()
	password = data.get('password', '').strip()
	firstName = data.get('firstName', '').strip()
	lastName = data.get('lastName', '').strip()
	email = data.get('email', '').strip()

	User.objects.create_user(
		username=username,
		email=email,
		password=password,
		first_name=firstName,
		last_name=lastName,
	)

	payload = {}
	if username and password:
		# Test username/password combination
		user = auth.authenticate(username=username, password=password)
		# Found a match
		if user is not None:
			auth.login(request, user)
			payload['result'] = 'SUCCESS'
			payload['message'] = 'You\'re logged in'
		else:
			payload['result'] = 'FAILURE'
			payload['message'] = 'Login Failed, Please check your credentials'

		return HttpResponse(json.dumps(payload), content_type='application/json')

	return HttpResponseBadRequest()
