import json
from django.http import HttpResponse
from django.http import HttpResponseBadRequest

def getUserRoomsList(request):
	if request.user.is_authenticated:
		roomsData = [{
			'roomURI': r.roomURI,
			'name': r.name,
		} for r in request.user.room_set.all()]

		return HttpResponse(json.dumps({
			'rooms': roomsData,
		}), content_type='application/json')
	else:
		return HttpResponseBadRequest()
