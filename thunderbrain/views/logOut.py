import json
from django.contrib import auth
from django.http import HttpResponse

def logOut(request):
	if not request.user.is_authenticated:
		return HttpResponse()

	auth.logout(request)
	return HttpResponse(json.dumps({
		'result': 'LOGOUT'
	}))
