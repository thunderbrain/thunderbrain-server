from django.http import HttpResponse
from django.http import HttpResponseBadRequest

def getUserInfo(request):
	if request.user.is_authenticated:
		return HttpResponse({
			'userInfo': {
				'firstName': request.user.first_name,
				'lastName': request.user.last_name,
			},
		})
	else:
		return HttpResponseBadRequest()
