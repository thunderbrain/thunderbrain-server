import json
from django.contrib import auth
from django.http import HttpResponse, HttpResponseBadRequest
from django.views.decorators.csrf import csrf_exempt

@csrf_exempt
def login(request):
	if request.method != 'POST':
		return HttpResponseBadRequest()

	try:
		data = json.loads(request.body)
	except json.JSONDecodeError:
		return HttpResponseBadRequest()

	username = data.get('username', '').strip()
	password = data.get('password', '').strip()
	payload = {}
	if username and password:
		# Test username/password combination
		user = auth.authenticate(username=username, password=password)
		# Found a match
		if user is not None:
			auth.login(request, user)
			payload['result'] = 'SUCCESS'
			payload['message'] = 'You\'re logged in'
		else:
			payload['result'] = 'FAILURE'
			payload['message'] = 'Login Failed, Please check your credentials'

		return HttpResponse(json.dumps(payload), content_type='application/json')

	return HttpResponseBadRequest()
