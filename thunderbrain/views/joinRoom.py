import json
from django.http import HttpResponse, HttpResponseBadRequest

from thunderbrain.models import Room

def joinRoom(request):
	if request.method != 'POST' or not request.user.is_authenticated:
		return HttpResponseBadRequest()

	response = {}
	try:
		data = json.loads(request.body)
	except json.JSONDecodeError:
		return HttpResponseBadRequest()
	try:
		response['result'] = 'ROOM_JOINED'

		if data.get('roomURI'):
			roomURI = data['roomURI']
			room = Room.objects.get(roomURI=roomURI)

		elif data.get('password'):
			room = Room()
			room.adminPassword = data.get('password')
			room.save()
			response['result'] = 'ROOM_CREATED'

		else:
			raise Exception('wrong join request')

		response['roomURI'] = room.roomURI
		response['roomName'] = room.name

		if request.user not in room.users.all():
			room.users.add(request.user)
			room.save()

		return HttpResponse(
			json.dumps(response),
			content_type='application/json',
			status=200,
		)

	except Exception as e:
		print(f'joinRoom exception, {e}')
		return HttpResponseBadRequest()
