from background_task import background
from django.utils.timezone import now, timedelta
from thunderbrain.models import Room
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer

# it's possible to make background tasks async - not sure if that's a good idea.
@background(schedule=now()+timedelta(seconds=120))
def proceedRoomPhaseTask(roomID, phase):
    print(f"[BG task] {roomID} task")
    room = Room.objects.get(roomID=roomID)
    if room.phase == phase:
        roomGroup = room.getRoomGroupName()
        room.proceed()
        room.save()

        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send(
            roomGroup,
            {
                'event': 'roomPhaseChanged',
                'payload': {
                    'newPhase': room.phase,
                    #FIXME decide either now or schedule
                    'timestamp': str(now()),
                }
            }
        ))
    else:
        print(f"[BG task] {roomID} phase changed during the task - ignoring the task")


