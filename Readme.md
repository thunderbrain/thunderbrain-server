# Thunderbrain-server
The backend for the Thunderbrain brainstorming platform.

<!-- MarkdownTOC -->

- [Prerequisites](#prerequisites)
	- [Python 3.6 and PIP](#python-36-and-pip)
		- [Linux](#linux)
		- [Windows](#windows)
	- [Pipenv](#pipenv)
- [Dependencies](#dependencies)
- [Running](#running)
- [Testing](#testing)

<!-- /MarkdownTOC -->

<a id="prerequisites"></a>
## Prerequisites
* Python 3.6 and PIP
* pipenv

<a id="python-36-and-pip"></a>
### Python 3.6 and PIP
<a id="linux"></a>
#### Linux

To verify installed Python 3 version, run:
```sh
python3 --version
```

If it's `3.6.x`, you're good to go.

If unavailable, you can either install it from your distribution's repository or use Pyenv.
Ubuntu 18.04 and derivatives provide Python 3.6 as the default for Python 3, so installation boils down to:

```sh
sudo apt install python3 python3-pip
```

Others (including e.g. ArchLinux) provide newer or older versions.
In this case, use Pyenv:

1. Install Pyenv:
	1. Download the Pyenv itself:
		`curl https://pyenv.run | $SHELL`
	2. Add it to your `$PATH` (change `.bashrc` to your shell's configuration file, e.g. `.zshrc`):
		`echo "export PATH=$HOME/.pyenv/bin:$PATH" >> ~/.bashrc`
	3. Open new terminal window or re-source your shell's configuration file
2. Install Python 3.6.8 using Pyenv:
	`pyenv install 3.6.8`
3. Profit - Pipenv will automatically detect and use the locally-installed Python 3.6!

<a id="windows"></a>
#### Windows
* Option 1 (preferred): If on Windows 10, [install Ubuntu 18.04 from Microsoft Store](https://www.microsoft.com/en-us/p/ubuntu/9nblggh4msv6) (notice the prerequisite of enabling WSL) and continue using instructions for Linux.
* Option 2 (little more hassle): Get and install Python 3.6 from [the official page](https://www.python.org/downloads/)

<a id="pipenv"></a>
### Pipenv
First, install Pipenv for the local user:
```sh
pip3 install --user pipenv
```

Then, set one very important environment variable add Pipenv it to your `$PATH`:
```sh
# for Bash:
echo "export PIPENV_VENV_IN_PROJECT=True" >> ~/.bashrc
echo "export PATH=$HOME/.local/bin:$PATH" >> ~/.bashrc
# or for ZSH:
echo "export PIPENV_VENV_IN_PROJECT=True" >> ~/.zshrc
echo "export PATH=$HOME/.local/bin:$PATH" >> ~/.zshrc
# (or just use the pipenv plugin for oh-my-zsh)
```

Then, open a new terminal window or re-source your shell's configuration file:
```sh
source ~/.bashrc
# Or
```

<a id="dependencies"></a>
## Dependencies
Just enter the main directory and run
```sh
pipenv install
```
That's it!

<a id="running"></a>
## Running
1. Prepare your database:
	1. `pipenv run makemigrations thunderbrain`
	2. `pipenv run migrate`
2. Create superuser and collect static files (only needed on the first run):
	1. `pipenv run createsuperuser` (admin/admin works for local/development purposes)
	2. `pipenv run collectstatic`
3. Run redis:
	1. Easy mode aka docker:
		1. `docker run -p 6379:6379 -d redis:latest`
	2. Hard mode aka manually and system-wise:
		1. `sudo apt install redis`
		2. edit `/etc/redis.conf`
		3. `sudo systemctl enable redis`
		4. `sudo systemctl restart redis`
3. Run the development server:
	1. `pipenv run server`

<a id="testing"></a>
## Testing
First, install development dependencies:
```sh
pipenv install --dev
```

Then you can run following tests:
1. Linters:
    1. pylint: `pipenv run pylint`
    2. flake8: `pipenv run flake8`
2. Unit tests:
    1. django/unittest: `pipenv run test`
