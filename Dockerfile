FROM mc706/pipenv-3.6

ENV PATH "$PATH:/root/.local/bin"
ENV PIPENV_VENV_IN_PROJECT true

WORKDIR /thunderbrain-server

COPY Pipfile* ./

RUN pipenv install

COPY . .

RUN pipenv run migrate
RUN pipenv run collectstatic
RUN echo "from django.contrib.auth import get_user_model; User = get_user_model(); User.objects.create_superuser('admin', 'admin@thunderbrain.com', 'admin')" | pipenv run python manage.py shell

EXPOSE 8001

CMD [ "pipenv", "run", "server_prod" ]
